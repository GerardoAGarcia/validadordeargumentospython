# -*- coding: UTF-8 -*-
import sys
## Se importa el archivo que contiene la clase para ser instanciada.
import Migrated as Mg
migrated = Mg.Migrated()
## Se instancia el objeto de la clase importada a nivel global
contadorArgumentos=0
prefijos=[]
    ## Valida que los argumentos introducidos correspondan al universo de prefijos
def validaArgumentoMigrador(argumento):
    switcher = {
    "-t": True,
    "-pi": True,
    }
    result=switcher.get(argumento,False)
    return result
    ## Método donde deberían ir los setter de los atributos del objeto de acuerdo al prefijos
def asignaValor(prefijo,valor):
    global migrated
    switch = {
    "-t": migrated.setNombreTabla,
    "-pi": migrated.setPIAnterior,
    }
    return switch[prefijo](valor)
    ## Método que valida los argumentos recibidos, los asocia a un prefijo y asigna 
    ## o settea el valor correspondiente.
def buscaPrefijosRepetidos(argumento, f):
    for prefijo in prefijos:
        if prefijo==argumento:
            print("Existe un argumento repetido --> "+argumento)
            sys.exit()
        
def validaArgumentos(argumentosLen, f,f2):
    global contadorArgumentos
    global prefijos
    if argumentosLen>1:
        i=0
        argumentos = sys.argv
        ## Iteración de los argumentos recibidos
        for argumento in argumentos:
            result=False
            if i>=1:
                ## Se analizan los parametros impares para validar los prefijos
                if i%2>0:
                    buscaPrefijosRepetidos(argumento, f)
                    result=f(argumento)
                    if result==False:
                        print("--> No se reconoce el prefijo: "+argumento)
                        sys.exit()
                    else:
                        ## Se verifica que exista un argumento superior al indicado 
                        ## de lo contrario notifica que falta un valor para el prefijo
                        if argumentosLen<=i+1:
                            print ("--> Falta argumento para: "+argumentos[i])
                            sys.exit()
                        else:
                            ## Una vez pasada la validación se manda llamar el setter del 
                            ## objeto instanciado de acuerdo al prefijo.
                            prefijos.append(argumento)
                            f2(argumento,argumentos[i+1])
                            contadorArgumentos+=1                           
            i+=1
    else:
        print("Faltan argumentos")
        sys.exit()
    ## imprime banner TEST
def imprimirArgumentosNecesarios():
    print("-t nombre_tabla o objeto\n -pi PI Anterior")
def banner():
    print(" _____ _____ ____ _____ \n|_   _| ____/ ___|_   _|\n  | | |  _| \___ \ | |  \n  | | | |___ ___) || |  \n  |_| |_____|____/ |_|  \n                       ")
