# -*- coding: UTF-8 -*-
    ## Se importa el módulo de validación
import module_validation as mv
import sys
def main():
    mv.banner()
    mv.validaArgumentos(len(sys.argv),mv.validaArgumentoMigrador,mv.asignaValor)
    ## Valida el número de argumentos mínimos necesarios para el funcionamiento del programa
    ## de lo contrario termina.
    if mv.contadorArgumentos < 2 :
        print("Faltan argumentos: ")
        mv.imprimirArgumentosNecesarios()
    else:
        print("Continúa el programa...")
if __name__ == "__main__":
    main()