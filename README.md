# TEST Arguments V1.0 Release

## Introduction

>This Script let you validate the parameters in your Python script.

## Code Samples

> The source code is in the next address <br>
<strong>https://gitlab.com/GerardoAGarcia/validadordeargumentospython.git </strong><br>
You can clone this 


## Configuration

>--> You need make some simple modifications in the structure program script<br>
>--> Define your own object with the correct setter methods<strong>replace Migrated.py and replace the importa name in en module_validation.py</strong>
> You need make modification in the methods validaArgumentoMigrador and asignaValor in <strong> module_validation.py </strong> with the correct flag prefix parameter.
> Define the flag to validate
def validaArgumentoMigrador(argumento):
    switcher = {
    "-t": True,
    "-pi": True,
    }
    result=switcher.get(argumento,False)
    return result
> Define the correct setter method for flag
def asignaValor(prefijo,valor):
    global migrated
    switch = {
    "-t": migrated.setNombreTabla,
    "-pi": migrated.setPIAnterior,
    }
    return switch[prefijo](valor)

### Credits
By Gerardo A Garcia (Jerry)