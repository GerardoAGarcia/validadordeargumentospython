# -*- coding: UTF-8 -*-
    ## Definición del objeto a utilizar
import sys
import module_validation as mv
class Migrated:
    nombre_tabla=""
    pi_anterior=0
    def setNombreTabla(self, nombre_tabla):
        self.nombre_tabla=nombre_tabla
    def setPIAnterior(self,pi_anterior):
        try:
            self.pi_anterior=int(pi_anterior)
        except:
            print("Se esperaba un valor entero para PI Anterior")
            mv.imprimirArgumentosNecesarios()
            sys.exit()
    def __str__(self):
        return "Nombre tabla: "+self.nombre_tabla+" PI Anterior: "+str(self.pi_anterior)